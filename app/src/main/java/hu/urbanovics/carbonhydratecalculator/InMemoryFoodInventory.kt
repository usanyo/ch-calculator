package hu.urbanovics.carbonhydratecalculator

import hu.urbanovics.carbonhydratecalculator.foodcalculator.FoodInventory

class InMemoryFoodInventory : FoodInventory {
    override val foods: List<FoodDto>
        get() = listOf(
            FoodDto("Zabpehely", "58,5"),
            FoodDto("Chips", "50"),
            FoodDto("Narancs", "10"),
            FoodDto("Tej", "4,5"),
            FoodDto("Alma", "7")
        )

}
