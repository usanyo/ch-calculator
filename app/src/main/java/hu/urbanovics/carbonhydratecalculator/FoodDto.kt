package hu.urbanovics.carbonhydratecalculator

data class FoodDto(val name: String, val chIn100g: String)
