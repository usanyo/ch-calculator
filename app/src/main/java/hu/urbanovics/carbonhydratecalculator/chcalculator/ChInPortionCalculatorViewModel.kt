package hu.urbanovics.carbonhydratecalculator.chcalculator

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import hu.urbanovics.carbonhydratecalculator.core.ChInPortionCalculatorPage

class ChInPortionCalculatorViewModel: ViewModel(),
    ChInPortionCalculatorPage {
    private lateinit var chIn100gCallback: (String) -> Unit
    private lateinit var weightOfFoodCallback: (String) -> Unit

    val chInPortion: MutableLiveData<String> = MutableLiveData("")

    fun chIn100gChanged(text: CharSequence, start: Int, before: Int, count: Int) {
        chIn100gCallback(text.toString())
    }

    fun weightOfFoodChanged(text: CharSequence, start: Int, before: Int, count: Int) {
        weightOfFoodCallback(text.toString())
    }

    override fun onChIn100gChanged(callback: (chIn100g: String) -> Unit) {
        chIn100gCallback = callback
    }

    override fun onWeightOfFoodChanged(callback: (weightOfFood: String) -> Unit) {
    }

    override var amountOfChInPortion: String
        get() = chInPortion.value ?: ""
        set(value) {
            chInPortion.value = value
        }
}