package hu.urbanovics.carbonhydratecalculator.chcalculator

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import hu.urbanovics.carbonhydratecalculator.R
import hu.urbanovics.carbonhydratecalculator.databinding.ChCalculatorPageBinding

class ChCalculatorFragment(private val viewModel: ChInPortionCalculatorViewModel) : Fragment() {
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        val binding: ChCalculatorPageBinding =
            DataBindingUtil.inflate(inflater, R.layout.ch_calculator_page, container, false)
        binding.lifecycleOwner = this
        binding.viewmodel = viewModel
        return binding.root
    }
}