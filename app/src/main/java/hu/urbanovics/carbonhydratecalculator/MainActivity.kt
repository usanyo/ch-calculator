package hu.urbanovics.carbonhydratecalculator

import android.os.Bundle
import androidx.fragment.app.FragmentActivity
import androidx.viewpager.widget.ViewPager
import hu.urbanovics.carbonhydratecalculator.chcalculator.ChCalculatorFragment
import hu.urbanovics.carbonhydratecalculator.chcalculator.ChInPortionCalculatorViewModel
import hu.urbanovics.carbonhydratecalculator.core.CarbonHydrateCalculatorApp
import hu.urbanovics.carbonhydratecalculator.foodcalculator.MealCalculatorViewModel
import hu.urbanovics.carbonhydratecalculator.foodcalculator.FoodCalculatorFragment

class MainActivity : FragmentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val mealCalculator = MealCalculatorViewModel()
        val chInPortionPage = ChInPortionCalculatorViewModel()
        val app = CarbonHydrateCalculatorApp(mealCalculator, chInPortionPage)
        app.setup()

        findViewById<ViewPager>(R.id.pager).adapter =
            SimplePagerAdapter(listOf(
                FoodCalculatorFragment(mealCalculator, InMemoryFoodInventory()),
                ChCalculatorFragment(chInPortionPage)
            ), supportFragmentManager)
    }
}
