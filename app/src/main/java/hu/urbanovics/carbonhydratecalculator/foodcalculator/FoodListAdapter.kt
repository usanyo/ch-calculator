package hu.urbanovics.carbonhydratecalculator.foodcalculator

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import hu.urbanovics.carbonhydratecalculator.R

class FoodListAdapter(private val foodInventory: FoodInventory): RecyclerView.Adapter<FoodListItemHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FoodListItemHolder {
        return FoodListItemHolder(LayoutInflater.from(parent.context).inflate(R.layout.food_list_item, parent, false))
    }

    override fun getItemCount(): Int = foodInventory.foods.size

    override fun onBindViewHolder(holder: FoodListItemHolder, position: Int) {
        holder.name = foodInventory.foods[position].name
        holder.chIn100g = foodInventory.foods[position].chIn100g
    }
}