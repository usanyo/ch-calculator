package hu.urbanovics.carbonhydratecalculator.foodcalculator

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import hu.urbanovics.carbonhydratecalculator.core.MealCalculatorPage

class MealCalculatorViewModel: ViewModel(), MealCalculatorPage {
    private lateinit var chIn100gCallback: (String) -> Unit
    private lateinit var desiredChCallback: (String) -> Unit

    val amountOfFood: MutableLiveData<String> = MutableLiveData("26")

    fun chIn100gChanged(text: CharSequence, start: Int, before: Int, count: Int) {
        chIn100gCallback(text.toString())
    }

    fun desiredAmountOfChChanged(text: CharSequence, start: Int, before: Int, count: Int) {
        desiredChCallback(text.toString())
    }

    override fun onChIn100gChanged(callback: (chIn100g: String) -> Unit) {
        chIn100gCallback = callback
    }

    override fun onDesiredAmountOfChChanged(callback: (desiredCh: String) -> Unit) {
        desiredChCallback = callback
    }

    override var desiredAmountOfFood: String
        get() = amountOfFood.value ?: ""
        set(value) {
            amountOfFood.value = value
        }
}