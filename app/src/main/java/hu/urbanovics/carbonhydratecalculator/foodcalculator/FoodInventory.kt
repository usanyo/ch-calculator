package hu.urbanovics.carbonhydratecalculator.foodcalculator

import hu.urbanovics.carbonhydratecalculator.FoodDto

interface FoodInventory {
    val foods: List<FoodDto>
}
