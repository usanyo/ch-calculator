package hu.urbanovics.carbonhydratecalculator.foodcalculator

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import hu.urbanovics.carbonhydratecalculator.R
import hu.urbanovics.carbonhydratecalculator.databinding.FoodCalculatorPageBinding

class FoodCalculatorFragment(
    private val viewModel: MealCalculatorViewModel,
    private val foodInventory: FoodInventory
) : Fragment() {
    private lateinit var recyclerView: RecyclerView
    private lateinit var viewAdapter: RecyclerView.Adapter<*>
    private lateinit var viewManager: RecyclerView.LayoutManager

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        val binding: FoodCalculatorPageBinding =
            DataBindingUtil.inflate(inflater, R.layout.food_calculator_page, container, false)
        binding.lifecycleOwner = this
        binding.viewmodel = viewModel
        viewManager = LinearLayoutManager(context)
        viewAdapter = FoodListAdapter(foodInventory)

        recyclerView = binding.root.findViewById<RecyclerView>(R.id.foodListContainer).apply {
            setHasFixedSize(true)
            layoutManager = viewManager
            adapter = viewAdapter
        }

        return binding.root
    }
}