package hu.urbanovics.carbonhydratecalculator.foodcalculator

import android.view.View
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import hu.urbanovics.carbonhydratecalculator.R

class FoodListItemHolder(private val view: View): RecyclerView.ViewHolder(view) {
    var name: String
    get() = view.findViewById<TextView>(R.id.name).text.toString()
    set(value) {
        view.findViewById<TextView>(R.id.name).text = value
    }

    var chIn100g: String
    get() = view.findViewById<TextView>(R.id.chIn100g).text.toString()
    set(value) {
        view.findViewById<TextView>(R.id.chIn100g).text = value
    }
}