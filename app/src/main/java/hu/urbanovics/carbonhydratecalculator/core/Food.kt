package hu.urbanovics.carbonhydratecalculator.core

class Food(private val carbonhydrateIn100g: Double) {
    fun getCarbonhydrateOf(amount: Double): Double {
        throwErrorIfTheFoodIsInvalid()

        return carbonhydrateIn100g * amount / 100.0
    }

    fun getAmountOfFood(amountOfCarbonHydrate: Double): Double {
        throwErrorIfTheFoodIsInvalid()

        return if (carbonhydrateIn100g != 0.0)
            amountOfCarbonHydrate * 100 / carbonhydrateIn100g
        else
            Double.POSITIVE_INFINITY
    }

    private fun throwErrorIfTheFoodIsInvalid() {
        if (carbonhydrateIn100g < 0 || carbonhydrateIn100g > 100)
            throw OutOfRangeError()
    }
}
