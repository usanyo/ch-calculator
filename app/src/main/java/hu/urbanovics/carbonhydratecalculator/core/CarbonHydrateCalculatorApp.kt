package hu.urbanovics.carbonhydratecalculator.core

import java.lang.Exception

class CarbonHydrateCalculatorApp(
    private val foodCalculatorPage: MealCalculatorPage,
    private val chInPortionCalculatorPage: ChInPortionCalculatorPage
) {
    private var chIn100g = 58.5
    private var desiredAmountOfCh = 15.0
    private var weightOfFood = 200.0

    fun setup() {
        setupFoodCalculatorPage()
        setupChCalculatorPage()
    }

    private fun setupChCalculatorPage() {
        chInPortionCalculatorPage.onChIn100gChanged(this::applyNewChIn100g)

        chInPortionCalculatorPage.onWeightOfFoodChanged { weightOfFood ->
            this.weightOfFood = weightOfFood.toDouble()
            updateUI()
        }
    }

    private fun updateUI() {
        val food = Food(chIn100g)
        foodCalculatorPage.desiredAmountOfFood = "%.2f".format(food.getAmountOfFood(desiredAmountOfCh))
        chInPortionCalculatorPage.amountOfChInPortion = "%.2f".format(food.getCarbonhydrateOf(weightOfFood))
    }

    private fun showUndefined() {
        foodCalculatorPage.desiredAmountOfFood = "-"
        chInPortionCalculatorPage.amountOfChInPortion = "-"
    }

    private fun setupFoodCalculatorPage() {
        foodCalculatorPage.onChIn100gChanged(this::applyNewChIn100g)

        foodCalculatorPage.onDesiredAmountOfChChanged { desiredCh ->
            try {
                desiredAmountOfCh = desiredCh.toDouble()
                updateUI()
            } catch (e: Exception) {
                showUndefined()
            }
        }
    }

    private fun applyNewChIn100g(chIn100g: String) {
        try {
            this.chIn100g = chIn100g.toDouble()
            updateUI()
        } catch (e: Exception) {
            showUndefined()
        }
    }
}