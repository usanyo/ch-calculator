package hu.urbanovics.carbonhydratecalculator.core

class OutOfRangeError : Exception("Amount of CH must be between 0-100g") {

}
