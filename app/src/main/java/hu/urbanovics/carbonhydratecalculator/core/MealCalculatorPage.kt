package hu.urbanovics.carbonhydratecalculator.core

interface MealCalculatorPage {
    fun onChIn100gChanged(callback: (chIn100g: String) -> Unit)
    fun onDesiredAmountOfChChanged(callback: (desiredCh: String) -> Unit)
    var desiredAmountOfFood: String
}