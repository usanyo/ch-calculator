package hu.urbanovics.carbonhydratecalculator.core

interface ChInPortionCalculatorPage {
    var amountOfChInPortion: String

    fun onChIn100gChanged(callback: (chIn100g: String) -> Unit)

    fun onWeightOfFoodChanged(callback: (weightOfFood: String) -> Unit)
}
