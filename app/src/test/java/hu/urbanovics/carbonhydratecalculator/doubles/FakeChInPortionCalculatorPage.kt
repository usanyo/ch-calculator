package hu.urbanovics.carbonhydratecalculator.doubles

import hu.urbanovics.carbonhydratecalculator.core.ChInPortionCalculatorPage

class FakeChInPortionCalculatorPage: ChInPortionCalculatorPage {
    lateinit var chIn100gCallback: (String) -> Unit
    lateinit var weightOfFoodCallback: (String) -> Unit

    override var amountOfChInPortion: String = ""

    override fun onChIn100gChanged(callback: (chIn100g: String) -> Unit) {
        chIn100gCallback = callback
    }

    override fun onWeightOfFoodChanged(callback: (weightOfFood: String) -> Unit) {
        weightOfFoodCallback = callback
    }
}