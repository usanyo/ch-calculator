package hu.urbanovics.carbonhydratecalculator.doubles

import hu.urbanovics.carbonhydratecalculator.core.MealCalculatorPage

internal class FakeMealCalculatorPage: MealCalculatorPage {
    lateinit var chIn100gCallback: (String) -> Unit
    lateinit var amountOfChCallback: (String) -> Unit

    override fun onChIn100gChanged(callback: (chIn100g: String) -> Unit) {
        chIn100gCallback = callback
    }

    override fun onDesiredAmountOfChChanged(callback: (desiredCh: String) -> Unit) {
        amountOfChCallback = callback
    }

    override var desiredAmountOfFood: String = ""
}