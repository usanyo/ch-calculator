package hu.urbanovics.carbonhydratecalculator.core

import io.kotlintest.shouldBe
import io.kotlintest.shouldThrowExactly
import io.kotlintest.specs.WordSpec
import io.kotlintest.tables.forAll
import io.kotlintest.tables.headers
import io.kotlintest.tables.row
import io.kotlintest.tables.table

class FoodTests : WordSpec({

    "#getCarbonhydrateOf" should {
        "multiply the carbon hydrate in 100g with the amount of the food and divide by 100" {
            table(
                headers("amount of CH in 100g", "amount of food", "expected CH"),
                row(13.45, 100.0, 13.45),
                row(0.0, 500.0, 0.0),
                row(72.3, 100.0, 72.3),
                row(36.0, 200.0, 72.0),
                row(25.0, 300.0, 75.0)
            ).forAll { chIn100g, amountOfFood, expectedCh ->
                val food = Food(chIn100g)

                food.getCarbonhydrateOf(amountOfFood) shouldBe expectedCh
            }
        }

        "When the CH in 100g food is out of range then throw out of range error" {
            table(
                headers("invalid CH value"),
                row(-0.01),
                row(100.1)
            ).forAll { invalidChValue ->
                shouldThrowExactly<OutOfRangeError> {
                    Food(invalidChValue).getCarbonhydrateOf(10.0)
                }
            }
        }
    }

    "#getAmountOfFood" should {
        "return the amount of food based on the desired CH" {
            table(
                headers("amount of CH in 100g food", "desired CH", "expected amount of food"),
                row(0.0, 0.0, Double.POSITIVE_INFINITY),
                row(0.0, 1.0, Double.POSITIVE_INFINITY),
                row(20.0, 40.0, 200.0),
                row(12.3, 12.3, 100.0),
                row(70.0, 35.0, 50.0)
            ).forAll { chIn100gFood, desiredCh, expected ->
                val food = Food(chIn100gFood)
                food.getAmountOfFood(desiredCh) shouldBe expected
            }
        }

        "When the CH in 100g food is out of range then throw out of range error" {
            table(
                headers("invalid CH value"),
                row(-0.01),
                row(100.1)
            ).forAll { invalidChValue ->
                shouldThrowExactly<OutOfRangeError> {
                    Food(invalidChValue).getAmountOfFood(10.0)
                }
            }
        }
    }
})