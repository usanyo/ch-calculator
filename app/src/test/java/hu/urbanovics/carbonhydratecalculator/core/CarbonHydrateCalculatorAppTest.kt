package hu.urbanovics.carbonhydratecalculator.core

import hu.urbanovics.carbonhydratecalculator.doubles.FakeChInPortionCalculatorPage
import hu.urbanovics.carbonhydratecalculator.doubles.FakeMealCalculatorPage
import io.kotlintest.data.forall
import io.kotlintest.should
import io.kotlintest.shouldBe
import io.kotlintest.specs.StringSpec
import io.kotlintest.tables.row
import io.mockk.spyk
import io.mockk.verify

internal class CarbonHydrateCalculatorAppTest : StringSpec() {
    init {
        "#setup" should {

            "subscribe on 'CH in 100g text changed' event" {
                val mealCalculatorPage = spyk(FakeMealCalculatorPage())
                val chInPortionCalculatorPage = spyk(FakeChInPortionCalculatorPage())
                val subject = CarbonHydrateCalculatorApp(mealCalculatorPage, chInPortionCalculatorPage)

                subject.setup()

                verify { mealCalculatorPage.onChIn100gChanged(any()) }
            }


            "when 'CH in 100g text changed' event happens" should {
                val mealCalculatorPage = spyk(FakeMealCalculatorPage())
                val chInPortionCalculatorPage = spyk(FakeChInPortionCalculatorPage())
                val subject = CarbonHydrateCalculatorApp(mealCalculatorPage, chInPortionCalculatorPage)
                subject.setup()
                mealCalculatorPage.amountOfChCallback("30")

                "update the desired amount of food based on CH in 100g" {
                    forall(
                        row("15", "200,00"), row("5", "600,00"), row("60", "50,00")
                    ) { validNumber, expectedAmount ->
                        mealCalculatorPage.chIn100gCallback(validNumber)

                        mealCalculatorPage.desiredAmountOfFood shouldBe expectedAmount
                    }
                }

                "when 0 CH is in 100g" should {
                    "set desired amount of food to '-'" {
                        mealCalculatorPage.chIn100gCallback("0,0")

                        mealCalculatorPage.desiredAmountOfFood shouldBe "-"
                    }
                }


                "when CH in 100g can't be parsed" should {
                    "set desiredAmountOfFood to '-'" {
                        forall(row(""), row("letters"), row("-0.1"), row("100,01")) { invalidNumber ->
                            mealCalculatorPage.chIn100gCallback(invalidNumber)

                            mealCalculatorPage.desiredAmountOfFood shouldBe "-"
                        }
                    }
                }
            }

            "subscribe on 'desired amount of food changed' event" {
                val mealCalculatorPage = spyk(FakeMealCalculatorPage())
                val chInPortionCalculatorPage = spyk(FakeChInPortionCalculatorPage())
                val subject = CarbonHydrateCalculatorApp(mealCalculatorPage, chInPortionCalculatorPage)

                subject.setup()

                verify { mealCalculatorPage.onDesiredAmountOfChChanged(any()) }
            }

            "when 'desired amount of food changed' event happens" should {
                val mealCalculatorPage = spyk(FakeMealCalculatorPage())
                val chInPortionCalculatorPage = spyk(FakeChInPortionCalculatorPage())
                val subject = CarbonHydrateCalculatorApp(mealCalculatorPage, chInPortionCalculatorPage)
                subject.setup()
                mealCalculatorPage.chIn100gCallback("10.0")

                "update the desired amount of food based on desired amount of CH" {
                    forall(
                        row("1", "10,00"),
                        row("12", "120,00"),
                        row("12.", "120,00"),
                        row("12.3", "123,00")
                    ) { validNumber, expectedResult ->
                        mealCalculatorPage.amountOfChCallback(validNumber)

                        mealCalculatorPage.desiredAmountOfFood shouldBe expectedResult
                    }
                }

                "when desired amount of CH can't be parsed" should {
                    "set desired amount of CH to '-'" {
                        forall(row(""), row("letters")) { invalidNumber ->
                            mealCalculatorPage.amountOfChCallback(invalidNumber)

                            mealCalculatorPage.desiredAmountOfFood shouldBe "-"
                        }
                    }
                }
            }

            "subscribe on 'CH in 100g text changed' event of CH calculator page" {
                val mealCalculatorPage = spyk(FakeMealCalculatorPage())
                val chInPortionCalculatorPage = spyk(FakeChInPortionCalculatorPage())
                val subject = CarbonHydrateCalculatorApp(mealCalculatorPage, chInPortionCalculatorPage)

                subject.setup()

                verify { chInPortionCalculatorPage.onChIn100gChanged(any()) }
            }

            "when 'CH in 100g text changed' event happens on CH calculator page" should {
                val mealCalculatorPage = spyk(FakeMealCalculatorPage())
                val chInPortionCalculatorPage = spyk(FakeChInPortionCalculatorPage())
                val subject = CarbonHydrateCalculatorApp(mealCalculatorPage, chInPortionCalculatorPage)
                subject.setup()
                chInPortionCalculatorPage.weightOfFoodCallback("200")

                "update the amount of CH in portion based on CH in 100g" {
                    forall(
                        row("15", "30,00"), row("5", "10,00"), row("60", "120,00")
                    ) { validNumber, expectedAmount ->
                        chInPortionCalculatorPage.chIn100gCallback(validNumber)

                        chInPortionCalculatorPage.amountOfChInPortion shouldBe expectedAmount
                    }
                }
            }

            "subscribe on 'weight of food text changed' event of CH calculator page" {
                val mealCalculatorPage = spyk(FakeMealCalculatorPage())
                val chInPortionCalculatorPage = spyk(FakeChInPortionCalculatorPage())
                val subject = CarbonHydrateCalculatorApp(mealCalculatorPage, chInPortionCalculatorPage)

                subject.setup()

                verify { chInPortionCalculatorPage.onWeightOfFoodChanged(any()) }
            }

            "when 'weight of food changed' event happens" should {
                val mealCalculatorPage = spyk(FakeMealCalculatorPage())
                val chInPortionCalculatorPage = spyk(FakeChInPortionCalculatorPage())
                val subject = CarbonHydrateCalculatorApp(mealCalculatorPage, chInPortionCalculatorPage)
                subject.setup()
                chInPortionCalculatorPage.chIn100gCallback("10.0")

                "update the amount of CH in the portion" {
                    forall(
                        row("10", "1,00"),
                        row("120", "12,00"),
                        row("120.", "12,00"),
                        row("120.3", "12,03")
                    ) { validNumber, expectedResult ->
                        chInPortionCalculatorPage.weightOfFoodCallback(validNumber)

                        chInPortionCalculatorPage.amountOfChInPortion shouldBe expectedResult
                    }
                }
            }
        }
    }
}

